from unittest import TestCase, main

from sqlalchemy.sql.expression import false
import random

from app import app

class TestSongQueries(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.test_app = app.test_client()
from app.init_db import Artist, Album, Song
from os import environ

from unittest import TestCase, main

from sqlalchemy import create_engine, asc, desc
from sqlalchemy.orm import sessionmaker


pg_addr = f"postgresql+psycopg2://{environ['SQPG_ADDR']}" 
engine = create_engine(pg_addr)
Session = sessionmaker(bind=engine)
session = Session()

class SongDBTest(TestCase):

    #####################
    # Test: /song?<...> #
    #####################

    def test_filt1(self):
        res = session.query(Song)
        self.assertEqual(len(res.all()), 36914)

    def test_filt2(self):
        res = session.query(Song)
        res = res.order_by(desc(Song.name))
        res = res.offset(100).limit(20)

        self.assertEqual(len(res.all()), 20)
        self.assertTrue(res.first().name >= res.all()[19].name)

    def test_filt3(self):        
        res = session.query(Song).filter(Song.name.ilike(r"%song%"))

        self.assertEqual(len(res.all()), 332)

        res = res.limit(5)

        self.assertEqual(len(res.all()), 5)
        for a in res.all():
            self.assertTrue('song' in a.name.lower())

    #########################
    # Test: /song/<song_id> #
    #########################

    def test_spec1(self):
        art = session.query(Song).filter(Song.id == 1).first()
        self.assertEqual(art.name, 'Curtains Up (Skit)')

    def test_spec2(self):
        art = session.query(Song).filter(Song.id == 18181).first()
        self.assertEqual(art.name, 'Almost Always True')

    def test_spec3(self):
        art = session.query(Song).filter(Song.id == 33001).first()
        self.assertEqual(art.name, "Move On")

class AlbumDBTest(TestCase):

    ######################
    # Test: /album?<...> #
    ######################

    def test_filt1(self):
        res = session.query(Album)
        self.assertEqual(len(res.all()), 2255)

    def test_filt2(self):
        res = session.query(Album)
        res = res.order_by(desc(Album.name))
        res = res.offset(100).limit(20)

        self.assertEqual(len(res.all()), 20)

        self.assertTrue(res.first().name >= res.all()[19].name)

    def test_filt3(self):        
        res = session.query(Album).filter(Album.name.ilike(r"%ath%"))

        self.assertEqual(len(res.all()), 24)

        res = res.limit(5)

        self.assertEqual(len(res.all()), 5)
        for a in res.all():
            self.assertTrue('ath' in a.name.lower())

    ###########################
    # Test: /album/<album_id> #
    ###########################

    def test_spec1(self):
        art = session.query(Album).filter(Album.id == 1).first()
        self.assertEqual(art.name, 'Encore')

    def test_spec2(self):
        art = session.query(Album).filter(Album.id == 333).first()
        self.assertEqual(art.name, 'Bootleg Recordings 1963')

    def test_spec3(self):
        art = session.query(Album).filter(Album.id == 1338).first()
        self.assertEqual(art.name, "Garage Inc.")

class ArtistDBTest(TestCase):

    #######################
    # Test: /artist?<...> #
    #######################

    def test_filt1(self):
        res = session.query(Artist)
        self.assertEqual(len(res.all()), 194)

    def test_filt2(self):
        res = session.query(Artist)
        res = res.order_by(desc(Artist.name))
        res = res.offset(100).limit(20)

        self.assertEqual(len(res.all()), 20)
        self.assertTrue(res.first().name >= res.all()[19].name)

    def test_filt3(self):        
        res = session.query(Artist).filter(Artist.name.ilike(r"%al%"))

        self.assertEqual(len(res.all()), 12)

        res = res.limit(5)

        self.assertEqual(len(res.all()), 5)
        for a in res.all():
            self.assertTrue('al' in a.name.lower())

    #############################
    # Test: /artist/<artist_id> #
    #############################

    def test_spec1(self):
        art = session.query(Artist).filter(Artist.id == 1).first()
        self.assertEqual(art.name, 'Eminem')

    def test_spec2(self):
        art = session.query(Artist).filter(Artist.id == 55).first()
        self.assertEqual(art.name, 'The Killers')

    def test_spec3(self):
        art = session.query(Artist).filter(Artist.id == 172).first()
        self.assertEqual(art.name, "Guns n' Roses")


if __name__ == "__main__":
    main()