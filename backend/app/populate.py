from sqlalchemy.sql.sqltypes import String
from init_db import session, create_all, drop_all, Artist, Album, Song
import json
import os
import re
import string

# Do some lazy cleaning
drop_all()
create_all()

# Load json dump
dump_addr = os.path.join(os.path.dirname(__file__), '../data/artist_data.json') 
with open(dump_addr, encoding="utf-8") as f:
    dump = json.load(f)

# Create filters
album_filter = re.compile(".*(live!|anthology|compilation|greatest hits|live (in|at|from)|collection|unreleased|collector's|studio|/|remix|remixed|world tour).*")

def timeParse(time):
    if not time:
        return -1
    seconds = 0
    for part in time.split(':'):
        seconds= seconds*60 + int(part)
    return seconds

# Create artist table
print('=== Adding Artists ===')
for artist in dump:
    art_name = artist['artistName']
    art_pic = artist['artistPictureURL']
    art_scnt = 0
    art_genre = 'None'
    art_actf = 9999

    albs = set()
    
    art_num_albums = len(artist['albumInfo'])
    for album in artist['albumInfo']:
        title = album['albumTitle'].lower().strip()
        title = title.translate(str.maketrans('', '', string.punctuation))

        if title in albs or album_filter.match(title):
            continue
        
        albs.add(title)
        art_scnt += len(album['albumTracks'])
        if art_genre == 'None':
            art_genre = album['albumGenre']
        if album['albumYear'] < art_actf and album['albumYear'] > 0:
            art_actf = album['albumYear']

    art = Artist(
        name=art_name,
        main_genre=art_genre,
        picture=art_pic,
        num_songs=art_scnt,
        active_from=art_actf,
        num_albums=art_num_albums
    )

    session.add(art)

session.commit()

# Create album table
print('=== Adding Albums ===')
for artist in dump:
    art = session.query(Artist).filter(Artist.name == artist['artistName']).first()
    art_id = art.id
    art_name = art.name

    albs = set()
    for album in artist['albumInfo']:
        title = album['albumTitle'].lower().strip()
        title = title.translate(str.maketrans('', '', string.punctuation))

        if title in albs or album_filter.match(title):
            continue
        
        albs.add(title)

        alb_name = album['albumTitle']
        alb_year = album['albumYear']
        alb_genre = album['albumGenre']
        alb_image = album['albumImage']
        song_count = len(album['albumTracks'])

        alb = Album(
            artist_id=art_id,
            artist_name=art_name,
            name=alb_name,
            genre=alb_genre,
            year=alb_year,
            picture=alb_image,
            num_songs=song_count
        )

        session.add(alb)

session.commit()

# Create song table
print('=== Adding Songs ===')
for artist in dump:
    art = session.query(Artist).filter(Artist.name == artist['artistName']).first()
    art_id = art.id
    art_name = art.name

    albs = set()
    for album in artist['albumInfo']:
        # Ignore certain albums
        title = album['albumTitle'].lower().strip()
        title = title.translate(str.maketrans('', '', string.punctuation))
        if title in albs or album_filter.match(title):
            continue
        albs.add(title)
        alb = session.query(Album).filter(Album.name == album['albumTitle'] and 
            Album.artist_id == art_id).first()
        alb_id = alb.id
        alb_name = alb.name
        song_genre = album['albumGenre']
        for song in album['albumTracks']:
            song_name = song['title']
            song_len = timeParse(song['duration'])
            song_vid = song['trackEmbedURL']

            sng = Song(
                artist_id=art_id,
                artist_name=art_name,
                album_id=alb_id,
                album_name=alb_name,
                name=song_name,
                genre=song_genre,
                length=song_len,
                video_embed_url=song_vid
            )

            session.add(sng)

session.commit()

print('=== Done Populating ===')