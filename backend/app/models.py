class ArtistsData:
    def __init__(self, size, artists):
        """Returns data for multiple artists

            Parameters:
            size (int): number of artists returned
            artists (Artist): list of Artist objects
        """
        self.size = size
        self.artists = artists


class Artist:
    def __init__(self, id, name, mainGenre, albums, picture, numSongs, activeFrom):
        """Information about a specific artist

            Parameters:
            id (int): unique id for artist
            name (string): name of the artist
            mainGenre (string): primary genre of music that the artist produces
            albums ([{int, string}]): list of objects containing each albums' unique id and its name
            picture (string): public url for image of artist
            numSongs (int): # of songs created by artist
            activeFrom (int): year the artist was active from
        """
        self.id = id
        self.name = name
        self.mainGenre = mainGenre
        self.albums = albums
        self.picture = picture
        self.numSongs = numSongs
        self.activeFrom = activeFrom


class SongsData:
    """Returns data for multiple songs

        Parameters:
        size (int): number of songs returned
        artists (Artist): list of Song objects
    """
    def __init__(self, size, songs):
        self.size = size
        self.songs = songs


class Song:
    """Information about a specific song

        Parameters:
        id (int): unique id for song
        name (string): name of the song
        genre (string): genre of the song
        album ({int, string}): object containing the album's unique id and its name
        length (int): length of the song in seconds
        artist ({int, string}): object containing the artist's unique id and his/her name
        videoEmbedURL (string): url for embedded video playback on sites such as Youtube
        spotifyEmbedURL (istringnt): url for embedded playback on Spotify
    """
    def __init__(self, id, name, genre, album, length, artist, videoEmbedURL, spotifyEmbedURL):
        self.id = id
        self.name = name
        self.genre = genre
        self.album = album
        self.length = length
        self.artist = artist
        self.videoEmbedURL = videoEmbedURL #Discogs has it
        self.spotifyEmbedURL = spotifyEmbedURL #Spotify has it


class AlbumsData:
    """Returns data for multiple albums

        Parameters:
        size (int): number of albums returned
        artists (Artist): list of Album objects
    """
    def __init__(self, size, albums):
        self.size = size
        self.albums = albums


class Album:
    """Information about a specific album

        Parameters:
        id (int): unique id for album
        name (string): name of the album
        genre (string): genre of the album
        songs ([{int, string}]): list of objects containing the songs's unique id and its name
        artist ({int, string}): object containing the artist's unique id and his/her name
        year (int): year the album was released
        spotifyEmbedURL (istringnt): url for embedded playback on Spotify
    """
    def __init__(self, id, name, genre, songs, artist, year, picture, spotifyEmbedURL):
        # int
        self.id = id
        # string
        self.name = name,
        # string
        self.genre = genre,
        # list of {id, name} where id is int and name is string
        self.songs = songs,
        # {id, name} where id is int and name is string
        self.artist = artist,
        # int, e.g. 2009
        self.year = year
        # cover art for the album
        self.picture = picture
        self.spotifyEmbedURL = spotifyEmbedURL
