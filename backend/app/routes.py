import json, requests
from os import pipe
import re
import math
import subprocess
from sqlalchemy.orm import defaultload
from sqlalchemy.sql.expression import all_
import sqlalchemy.sql.sqltypes as sqltypes
from sqlalchemy import asc, desc, func, or_
from functools import wraps
from subprocess import Popen, PIPE, check_output, run
from flask import request
from app import app
from flask import jsonify
from .models import Artist
from .init_db import session, Artist, Album, Song


def db_exception_handler(func):
    @wraps(func)
    def inner_function(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except:
            session.rollback()
            return "A database related exception has occurred"
    return inner_function


@app.route('/')
def index():
    return 'Welcome to the MusicMuse API!'

#takes about 10 seconds to run
@app.route('/test')
def test():
    process = subprocess.Popen(['python', 'test.py', '-v'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output = process.communicate()[0].decode("utf-8")
    return output


#################
# Query Helpers #
#################

def querySongs(q_str):
    if not q_str:
        return session.query(Song)
    else:
        # Add Postgres wildcards
        w_str = f"%{q_str}%"
        # Apply multiple filter passes
        return session.query(Song).filter(Song.name.ilike(w_str) |
                                          Song.artist_name.ilike(w_str) |
                                          Song.album_name.ilike(w_str) |
                                          Song.genre.ilike(w_str))

def queryAlbums(q_str):
    if not q_str:
        return session.query(Album)
    else:
        # Add Postgres wildcards
        w_str = f"%{q_str}%"
        # Apply multiple generic filters
        q_filts = [Album.name.ilike(w_str),
                    Album.artist_name.ilike(w_str),
                    Album.genre.ilike(w_str)]

        # Add numeric specific filters
        if q_str.isnumeric():
            q_filts.append(Album.year == q_str)

        # Unpack and or all the filters
        return session.query(Album).filter(or_(*q_filts))

def queryArtists(q_str):
    if not q_str:
        return session.query(Artist)
    else:
        # Add Postgres wildcards
        w_str = f"%{q_str}%"
        # Apply multiple generic filters
        q_filts = [Artist.name.ilike(w_str),
                    Artist.main_genre.ilike(w_str)]

        # Add numeric specific filters
        if q_str.isnumeric():
            q_filts.append(Artist.active_from == q_str)

        # Unpack and or all the filters
        return session.query(Artist).filter(or_(*q_filts))

##################
# Filter Helpers #
##################

def filterResults(tableClass, results, filter_str):
    if not filter_str:
        return results
    # Split the filter string into a list of words by the character |
    filter_list = filter_str.split('|')
    # Create a list of all the words in the filter string
    if len(filter_list) != 3:
        return results
    filter_arg = filter_list[0]
    filter_lowerbound = filter_list[1]
    filter_upperbound = filter_list[2]
    if not filter_lowerbound and not filter_upperbound:
        return results
    
    #get columns in the song table
    columnNames = [c.key for c in tableClass.__table__.columns]

    #get the type of each column in the song table
    types = [str(c.type) for c in tableClass.__table__.columns]

    validTypes = ['VARCHAR', 'INTEGER']

    if filter_arg in columnNames and types[columnNames.index(filter_arg)] in validTypes:
        col_attr = getattr(tableClass, filter_arg)
        #if the column type is string
        if types[columnNames.index(filter_arg)] == 'INTEGER':
            filter_lowerbound = int(filter_lowerbound) if filter_lowerbound else filter_lowerbound
            filter_upperbound = int(filter_upperbound) if filter_upperbound else filter_upperbound                   
        
        #filter the results using the filter_arg column and the filter_lowerbound and filter_upperbound bounds
        if filter_lowerbound:
            results = results.filter(col_attr >= filter_lowerbound)
        if filter_upperbound:
            results = results.filter(col_attr < filter_upperbound)
    return results

#################
# Bulk Fetchers #
#################


@app.route('/song')
@db_exception_handler
def getSongs():
    page_num = request.args.get('pageNum', default=1)
    per_page = request.args.get('perPage', default=25)
    sort_rule = request.args.get('sortRule', default='id')
    sort_dir = request.args.get('sortDir', default='asc')
    q_str = request.args.get('q', default='')
    filter_str = request.args.get('filter', default='')
    direction = desc if sort_dir == 'desc' else asc

    page_num = int(page_num)
    per_page = int(per_page)

    results = querySongs(q_str)
    results = filterResults(Song, results, filter_str)
    results = results.order_by(direction(getattr(Song, sort_rule)))

    #Don't do .limit(per_page) before doing this because this erases results from the query
    num_songs = results.count()
    results = results.offset(per_page * (page_num - 1)).limit(per_page)

    total_pages = math.ceil(float(num_songs) / float(per_page))

    packed_songs = [
        {
            'id': song.id,
            'name': song.name,
            'genre': song.genre,
            'length': song.length,
            'videoEmbedURL': song.video_embed_url,
            'spotifyEmbedURL': song.spotify_embed_url,
            'artist': {
                'id': song.artist.id,
                'name': song.artist.name
            },
            'album': {
                'id': song.album.id,
                'name': song.album.name
            }
        }
        for song in results
    ]

    send_response = {
        'songs': packed_songs,
        'page': page_num,
        'perPage': per_page,
        'sortRule': sort_rule,
        'sortDir': sort_dir,
        'totalPages': total_pages,
        'numResults': num_songs,
        'q': q_str,
        'filter': filter_str
    }

    return jsonify(send_response)


@app.route('/album')
@db_exception_handler
def getAlbums():
    page_num = request.args.get('pageNum', default=1)
    per_page = request.args.get('perPage', default=25)
    sort_rule = request.args.get('sortRule', default='id')
    sort_dir = request.args.get('sortDir', default='asc')
    q_str = request.args.get('q', default='')
    filter_str = request.args.get('filter', default='')
    direction = desc if sort_dir == 'desc' else asc

    page_num = int(page_num)
    per_page = int(per_page)

    results = queryAlbums(q_str)
    results = filterResults(Album, results, filter_str)
    results = results.order_by(direction(getattr(Album, sort_rule)))

    num_albums = results.count()
    results = results.offset(per_page * (page_num - 1)).limit(per_page)

    total_pages = math.ceil(float(num_albums) / float(per_page))

    packed_albums = [
        {
            'id': alb.id,
            'name': alb.name,
            'genre': alb.genre,
            'year': alb.year,
            'picture': alb.picture,
            'artist': {
                'id': alb.artist.id,
                'name': alb.artist.name
            },
            'songs': [{'id': s.id, 'name': s.name} for s in alb.songs]
        }
        for alb in results
    ]

    send_response = {
        'albums': packed_albums,
        'page': page_num,
        'perPage': per_page,
        'sortRule': sort_rule,
        'sortDir': sort_dir,
        'totalPages': total_pages,
        'numResults': num_albums,
        'q': q_str,
        'filter': filter_str
    }

    return jsonify(send_response)


@app.route('/artist')
@db_exception_handler
def getArtists():
    page_num = request.args.get('pageNum', default=1)
    per_page = request.args.get('perPage', default=25)
    sort_rule = request.args.get('sortRule', default='id')
    sort_dir = request.args.get('sortDir', default='asc')
    q_str = request.args.get('q', default='')
    filter_str = request.args.get('filter', default='')
    direction = desc if sort_dir == 'desc' else asc

    page_num = int(page_num)
    per_page = int(per_page)

    results = queryArtists(q_str)
    results = filterResults(Artist, results, filter_str)
    results = results.order_by(direction(getattr(Artist, sort_rule)))

    num_artists = results.count()
    results = results.offset(per_page * (page_num - 1)).limit(per_page)

    total_pages = math.ceil(float(num_artists) / float(per_page))

    packed_artists = [
        {
            'id': art.id,
            'name': art.name,
            'genre': art.main_genre,
            'activeFrom': art.active_from,
            'picture': art.picture,
            'numSongs': art.num_songs,
            'albums': [{'id': a.id, 'name': a.name} for a in art.albums]
        }
        for art in results
    ]

    send_response = {
        'artists': packed_artists,
        'page': page_num,
        'perPage': per_page,
        'sortRule': sort_rule,
        'sortDir': sort_dir,
        'totalPages': total_pages,
        'numResults': num_artists,
        'q': q_str,
        'filter': filter_str
    }

    return jsonify(send_response)

#####################
# Specific Fetchers #
#####################


@app.route('/song/<song_id>')
@db_exception_handler
def getSong(song_id):
    song = session.query(Song).filter(Song.id == song_id).first()

    response = {
        'id': song.id,
        'name': song.name,
        'genre': song.genre,
        'length': song.length,
        'videoEmbedURL': song.video_embed_url,
        'spotifyEmbedURL': song.spotify_embed_url,
        'artist': {
            'id': song.artist.id,
            'name': song.artist.name
        },
        'album': {
            'id': song.album.id,
            'name': song.album.name
        }
    }

    return jsonify(response)


@app.route('/album/<album_id>')
@db_exception_handler
def getAlbum(album_id):
    alb = session.query(Album).filter(Album.id == album_id).first()

    response = {
        'id': alb.id,
        'name': alb.name,
        'genre': alb.genre,
        'year': alb.year,
        'picture': alb.picture,
        'artist': {
            'id': alb.artist.id,
            'name': alb.artist.name
        },
        'songs': [{'id': s.id, 'name': s.name} for s in alb.songs]
    }

    return jsonify(response)


@app.route('/artist/<artist_id>')
@db_exception_handler
def getArtist(artist_id):
    art = session.query(Artist).filter(Artist.id == artist_id).first()

    response = {
        'id': art.id,
        'name': art.name,
        'genre': art.main_genre,
        'activeFrom': art.active_from,
        'picture': art.picture,
        'numSongs': art.num_songs,
        'albums': [{'id': a.id, 'name': a.name} for a in art.albums],
        'allSongs': [{'id': s.id, 'name': s.name} for s in art.songs]
    }

    return jsonify(response)


######################
# Group 8 API Caller #
######################

@app.route('/recipe')
def getRecipes():
    q_str = request.args.get('q', default='')
    jrec = requests.get('http://api-dot-friendly-autumn-317817.uc.r.appspot.com/api/recipes').json()

    # No query? return it all
    if len(q_str) == 0:
        ret = {**jrec, 'numRecipes': len(jrec['Recipes'])}
        return jsonify(ret)

    # Prune for ingredients
    ings = q_str.split(',')

    recipes = jrec['Recipes']

    main_list = []
    
    # Iterate recipes
    for rec in recipes:
        # Iterate recipe ingredients
        for ing in rec['recipeIngredients']:

            # Do inclusion check
            iname = ing['title'].lower()
            if all(asked.lower() in iname for asked in ings):
                main_list.append(rec)
                break

    ret = {'Recipes': main_list, 'numRecipes': len(main_list)}
    return jsonify(ret)

######################
# Group 6 API Caller #
######################

@app.route('/players')
def getPlayers():
    # Optional team select
    team_id = request.args.get('team_id', default='')

    # Get team info
    all_teams = requests.get('https://nbatoday.xyz/api/teams?per_page=30')
    if all_teams.status_code != 200:
        return jsonify({"teamMeta": {}, "topScorers": {}, "comment": "Group 6 API is down."})

    all_teams = all_teams.json()
    team_pack = {team['id']: {'name': team['name'], 'logo': team['logo_embed_link']}  for team in all_teams['data'] }

    # We are getting top scoring players

    # Create query string
    req_str = "per_page=20&sort_order=descending&sort_by=average_points_per_game"
    if team_id and team_id.isnumeric():
        req_str = f"{req_str}&current_team_id={team_id}"

    top_scorers = requests.get(f"https://nbatoday.xyz/api/players?{req_str}").json()

    ret_pack = {
        "teamMeta": team_pack,
        "topScorers": top_scorers,
        "comment": "Due to Group 8 API being down, we are using Group 6 API"
    }

    return jsonify(ret_pack)
    
    
