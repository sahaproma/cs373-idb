# cs373-idb

# URL: http://music-muse.me/

CS373 software engineering project

To run frontend:

- install node.js (my version is v12.18.4)
- in frontend dir, do "npm install"
- do "npm run"
- it should be running on localhost:3000

To run backend docker container:
- have docker running on machine (currently deployed in an EC2 instance on AWS)
- docker login (authenticate to docker hub)
- docker pull 2018kguo/musicmuse:flask
- do 'docker images' to see image ID
- do 'docker run -d -p 5000:5000 --env SQPG_ADDR={CLOUD_SQL_DB_CONNSTR} {IMAGE_ID}'
- Flask container will be running on machine on port 5000
