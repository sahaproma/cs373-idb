import React from 'react';
import NavBar from "./components/navbar";
import Footer from "./components/footer";
import {
  BrowserRouter as Router,
  Route,
  Switch
} from "react-router-dom";
import AboutPage from "./pages/About/About";
import SplashPage from './pages/SplashPage';
import Group8Page from './pages/Group8Page';
import AlbumsPage from './pages/Albums/Albums';
import ArtistsPage from './pages/Artists/Artists';
import TopSongsPage from './pages/Songs/TopSongs';
import SongPage from './pages/Songs/Song';
import ArtistPage from './pages/Artists/Artist';
import AlbumPage from './pages/Albums/Album';
import SearchPage from './pages/SearchPage';

export default function App() {
  return (
    <Router>
      <div className="background min-vh-100 d-flex flex-column">
        <div>
          <NavBar />
        </div>
        <div className='flex-fill d-flex h-auto background-jumbotron'>
          <Switch>
            <Route exact path="/">
              <SplashPage />
            </Route>
            <Route exact path="/about">
              <AboutPage />
            </Route>
            <Route exact path="/artists">
              <ArtistsPage />
            </Route>
            <Route exact path="/artists/:id">
              <ArtistPage />
            </Route>
            <Route exact path="/albums">
              <AlbumsPage />
            </Route>
            <Route exact path="/albums/:id">
              <AlbumPage />
            </Route>
            <Route exact path="/songs">
              <TopSongsPage />
            </Route>
            <Route exact path="/songs/:id">
              <SongPage />
            </Route>
            <Route exact path="/integration">
              <Group8Page />
            </Route>
            {/* look at BreathEasy apps.j line 63 when need to more */}
            <Route exact path="/search/:q">
              <SearchPage/>
            </Route>
          </Switch>
        </div>
        <div>
          <Footer />
        </div>
      </div>
    </Router>
  );
}
