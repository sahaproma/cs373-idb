import { configure, shallow } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import toJson from 'enzyme-to-json';
import React from 'react';
import { Nav } from 'react-bootstrap';
import NavBar from '../components/navbar';

test('Testing NavBar contains the proper links', () => {
  configure({ adapter: new Adapter() });
  const wrapper = shallow(<NavBar />);
  expect(wrapper.containsAllMatchingElements([
    <Nav.Link href='/about'>About</Nav.Link>,
    <Nav.Link href='/artists'>Artists</Nav.Link>,
    <Nav.Link href='/albums'>Albums</Nav.Link>,
    <Nav.Link href='/songs'>Top Songs</Nav.Link>
  ]));
});

test('Testing Navbar renders correctly', () => {
  configure({ adapter: new Adapter() });
  const wrapper = shallow(<NavBar />);
  expect(toJson(wrapper)).toMatchSnapshot();
});
