import Footer from '../components/footer'
import { shallow, configure } from 'enzyme'
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import toJson from 'enzyme-to-json';


test('Testing footer snapshot', () => {
    configure({ adapter: new Adapter() });
    const wrapper = shallow(<Footer />);
    expect(toJson(wrapper)).toMatchSnapshot();
    
});