import { configure, shallow } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import toJson from 'enzyme-to-json';
import React from 'react'
import EmbeddedPicture from '../components/embeddedPicture';

test('Test Embedded Picture', () => {
    configure({ adapter: new Adapter() })
    const wrapper = shallow(<EmbeddedPicture embedURL = "https://www.youtube.com/embed/v=4IoicMEEkBI"/>)
    expect(toJson(wrapper)).toMatchSnapshot();
});