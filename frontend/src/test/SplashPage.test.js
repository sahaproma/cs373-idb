import { render, screen } from '@testing-library/react';
import SplashPage from '../pages/SplashPage';

test('landing page text', () => {
  render(<SplashPage />);
  const pageText = screen.getByText(/MusicMuse/i);
  expect(pageText).toBeInTheDocument();
});

//test that the app contains a jumbotron with correct content
test('jumbotron contains correct content', () => {
  render(<SplashPage />);
  const jumbotron = screen.getByTestId('jumbotron');
  expect(jumbotron).toBeInTheDocument();
  const title = screen.getByText(/MusicMuse/i);
  expect(title).toBeInTheDocument();
  const subtitle = screen.getByText(/Music brought to you/i);
  expect(subtitle).toBeInTheDocument();
});


