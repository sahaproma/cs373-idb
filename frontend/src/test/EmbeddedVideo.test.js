import { configure, shallow } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import toJson from 'enzyme-to-json';
import React from 'react'
import EmbeddedVideo from '../components/embeddedVideo';

test('Test Embedded Video', () => {
    configure({ adapter: new Adapter() })
    const wrapper = shallow(<EmbeddedVideo embedURL = "https://www.youtube.com/embed/v=4IoicMEEkBI"/>)
    expect(toJson(wrapper)).toMatchSnapshot();
});