import React from "react";

const EmbeddedArtistsPicture = ({embedURL}) => {
    return (
        <a href={embedURL}><img alt="ArtistPicture" width = "200px" src={embedURL} ></img></a>
    )
}

// change stackoverflow to a relatable link
export default EmbeddedArtistsPicture;