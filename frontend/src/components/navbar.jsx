import React, { Component } from "react";
import { Navbar, Nav } from "react-bootstrap";
import SearchBar from './searchBar'
import '../styles/Navbar.css';


/* navigation bar for website */
const NavBar = () => {
  return (
      <Navbar expand="lg" className="navbar navbar-expand-lg navbar-custom navbar-dark">
        <Navbar.Brand href="/" className="nav-link">
          <b>MusicMuse</b>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
              <Nav.Link href="/about">About</Nav.Link>
              <Nav.Link href="/artists">Artists</Nav.Link>
              <Nav.Link href="/albums">Albums</Nav.Link>
              <Nav.Link href="/songs">Top Songs</Nav.Link>
              <Nav.Link href="/integration">API Integration</Nav.Link>
          </Nav>
        </Navbar.Collapse>
        <SearchBar></SearchBar>
      </Navbar>
  )
}

export default NavBar;
