import React, { useState } from 'react'
import { Table, Container, Button, Jumbotron } from "react-bootstrap";
import { useTable, usePagination } from 'react-table'

function DataTable({ columns, data, fetchData, pageCount: controlledPageCount }) {
    // Use the state and functions returned from useTable to build your UI
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        page,
        nextPage,
        previousPage,
        canNextPage,
        canPreviousPage,
        pageOptions,
        gotoPage,
        pageCount,
        setPageSize,
        // Get the state from the instance
        state: { pageIndex, pageSize },
        prepareRow,
    } = useTable({
        columns,
        data,
        manualPagination: true, // Tell the usePagination
        // hook that we'll handle our own data fetching
        // This means we'll also have to provide our own
        // pageCount.
        pageCount: controlledPageCount
    },
        usePagination,
    )

    const [pageInput, setPageInput] = useState(1)

    //Listen for changes in pagination and use the state to fetch our new data
    React.useEffect(() => {
        console.log(pageIndex + 1)
        fetchData({ pageNum: pageIndex + 1 })
    }, [pageIndex])

    // Render the UI for your table
    return (
        <>
            <Table striped bordered hover variant="dark" responsive style={{ margin: 0 }} {...getTableProps()}>
                <thead>
                    {headerGroups.map(headerGroup => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map(column => (
                                <th {...column.getHeaderProps()}>{column.render('Header')}</th>
                            ))}
                        </tr>
                    ))}
                </thead>
                <tbody {...getTableBodyProps()}>
                    {page.map((row, i) => {
                        prepareRow(row)
                        return (
                            <tr {...row.getRowProps()}>
                                {row.cells.map(cell => {
                                    return (
                                        <td {...cell.getCellProps()}>
                                            {cell.render('Cell')}
                                        </td>
                                    )
                                })}
                            </tr>
                        )
                    })}
                </tbody>
            </Table>

            {/* Pagination  */}

            <Jumbotron style={{ margin: 0 }}>
                <span>
                    Page{' '}
                    <strong>
                        {pageIndex + 1} of {pageOptions.length}
                    </strong>{' '}
                </span>
                <span>
                    | Go to page: {' '}
                    <input type='number' defaultValue={pageIndex + 1} min="1" onChange={e => {
                        const pageNumber = e.target.value ? Number(e.target.value) - 1 : 0
                        setPageInput(pageNumber)
                    }} style={{ width: '100px' }} /> <Button onClick={() => { gotoPage(pageInput) }}> Go To Page </Button>
                </span>
                <span> | <Button onClick={() => previousPage()} disabled={!canPreviousPage}>Previous</Button> <Button onClick={() => nextPage()} disabled={!canNextPage}>Next</Button></span>

                {/*< select value={pageSize} onChange={e => setPageSize(Number(e.target.value))}>
               {
                   [10, 25, 50].map(pageSize=>(
                       <option key={pageSize} value={pageSize}>
                          Show {pageSize} 
                       </option>
                   ))
               }
           </select> */}

                {/* update test data in front end */}

            </Jumbotron>
        </>
    )
}

export default DataTable