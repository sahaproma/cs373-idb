import React from "react";

/* footer for the website */
class Footer extends React.Component {
  render() {
    return (
      <div>
        <div className="bg-dark footer text-white text-center">Music For Everyone to Enjoy!</div>
      </div>
    );
  }
}

export default Footer;
