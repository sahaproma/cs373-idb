import React from 'react'
import { useEffect, useState } from 'react';
import { Image, Container, Jumbotron, Button, Toast, Spinner, Row, Dropdown, ListGroup } from "react-bootstrap";
import { useParams, useHistory } from "react-router-dom";
import axios from 'axios';

const AlbumPage = () => {
    const history = useHistory();
    const { id } = useParams();
    const [data, setData] = useState(null);

    useEffect(() => {
        fetchData()
    }, [])

    const fetchData = async () => {
        const res = await axios.get(`${process.env.REACT_APP_API_URL}/album/${id}`);
        console.log(res.data);
        setData(res.data);
     };

    function loadAlbumData() {
        return (
            <Container fluid style={{ margin: 0, padding: 0 }}>
                <Jumbotron>
                    {/* This is a album page, here is raw data for the album:
                    {JSON.stringify(data)} */}
                    <h1 className="name">{data.name}</h1> 
                    <Image src={data.picture}></Image>
                    <p>Artist: <a href={"../artists/" + data.artist.id}>{data.artist.name}</a> </p>
                    <p>Genre: {data.genre}</p>
                    <p>Year: {data.year > 0 ? data.year : "N/A"}</p>
                    <ListGroup>
                    {data.songs.map((elem) => (
                        <ListGroup.Item><a href={`/songs/${elem.id}`}> {elem.name} </a></ListGroup.Item>
                    ))}
                    </ListGroup>

                    <p style={{marginTop: 20}}>
                        <Button onClick={() => { history.push('/albums') }}>View All Albums</Button>
                    </p> 
                </Jumbotron>
            </Container>
        )
    }
    return (
        <Container fluid style={{ margin: 0, padding: 0 }}>
            {data ? loadAlbumData() : ""}
        </Container>
    )
}

export default AlbumPage;