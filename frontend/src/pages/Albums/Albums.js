import React from 'react'
import { useEffect, useState } from 'react';
import { Image, Container, Jumbotron, Button, Toast, Spinner, Row, Dropdown, Form, FormControl } from "react-bootstrap";
import Highlighter from "react-highlight-words";
import DataTable from '../../components/table';
import axios from 'axios';


const AlbumsPage = () => {
    const [data, setData] = useState([]);
    const [numPages, setNumPages] = useState(1);
    const [sortRule, setSortRule] = useState("id");
    const [sortDir, setSortDir] = useState("asc");
    const [curPage, setCurPage] = useState(1);
    const [filterString, setFilterString] = useState("");
    const [searchString, setSearchString] = useState("");
    const userInput = React.useRef();

    var fetchData = async ({ pageNum = curPage } = {}) => {
        var params = {
            pageNum: pageNum,
            perPage: 10,
            sortRule: sortRule,
            sortDir: sortDir,
            filter: filterString,
            q: searchString
        }
        const res = await axios.get(`${process.env.REACT_APP_API_URL}/album`, { params });
        console.log(res.data);
        setData(res.data.albums);
        setNumPages(res.data.totalPages);
        setCurPage(res.data.page);
    };

    function highlight(str) {
        return (
            <Highlighter
                highlightClassName="highlightClass"
                searchWords={[searchString]}
                autoEscape={true}
                textToHighlight={str}
            />
        )
    }


    React.useEffect(() => {
        fetchData()
    }, [sortDir, sortRule, filterString, searchString])

    function search() {
        window.location.assign("/search/q=" + userInput.current.value + "&filter=ALBUMS");
    }

    const columns = React.useMemo(
        () => [
            {
                Header: 'Albums',
                columns: [
                    {
                        Header: 'Title',
                        accessor: 'name',
                        Cell: (Row) => {
                            var elemId = Row.row.original.id
                            var name = Row.row.original.name
                            return (<a href={"/albums/" + elemId}>{highlight(name)}</a>)
                        },
                    },
                    {
                        Header: 'Artist',
                        accessor: 'artist',
                        Cell: ({ cell: { value } }) => {
                            if (value.id) {
                                //update this to /artists/ or /songs
                                return (<a href={"/artists/" + value.id}>{highlight(value.name)}</a>)
                            }
                            else {
                                return highlight(value.name)
                            }
                        }
                    },
                    {
                        Header: "Image",
                        accessor: "picture",
                        Cell: ({ cell: { value } }) => (
                            <Image
                                src={value}
                                width={150}
                                rounded
                            />
                        )
                    },
                    {
                        Header: 'Year',
                        accessor: 'year',
                        Cell: ({ cell: { value } }) => {
                            if (value <= 0) {
                                //update this to /artists/ or /songs
                                return ("N/A")
                            }
                            else {
                                return highlight(value.toString())
                            }
                        }
                    },
                    {
                        Header: 'Genre',
                        accessor: 'genre',
                        Cell: ({ cell: { value } }) => {
                            return highlight(value)
                        }
                    },
                    {
                        Header: 'Number of Songs',
                        accessor: 'songs',
                        Cell: ({ cell: { value } }) => {
                            return value.length
                        }
                    }
                ],
            }
        ],
        [searchString]
    )

    const NormalJumbo =
        <Jumbotron style={{ marginBottom: 0 }}>
            <h1>Browse All Albums</h1>
            <p>
                Find the best albums!
            </p>

            <Dropdown>
                <Dropdown.Toggle id="dropdown-basic" size="lg" variant="dark">
                    Sort By
                </Dropdown.Toggle>

                <Dropdown.Menu>
                    <Dropdown.Item onClick={() => { setSortRule("name"); setSortDir("asc") }}> Title (Asc.)</Dropdown.Item>
                    <Dropdown.Item onClick={() => { setSortRule("name"); setSortDir("desc") }}> Title (Desc.)</Dropdown.Item>

                    <Dropdown.Item onClick={() => { setSortRule("artist_name"); setSortDir("asc") }}> Artist (Asc.)</Dropdown.Item>
                    <Dropdown.Item onClick={() => { setSortRule("artist_name"); setSortDir("desc") }}> Artist (Desc.)</Dropdown.Item>

                    <Dropdown.Item onClick={() => { setSortRule("num_songs"); setSortDir("asc") }}> # of Songs (Asc.)</Dropdown.Item>
                    <Dropdown.Item onClick={() => { setSortRule("num_songs"); setSortDir("desc") }}> # of Songs (Desc.)</Dropdown.Item>

                    <Dropdown.Item onClick={() => { setSortRule("year"); setSortDir("asc") }}> Year (Asc.)</Dropdown.Item>
                    <Dropdown.Item onClick={() => { setSortRule("year"); setSortDir("desc") }}> Year (Desc.)</Dropdown.Item>

                    <Dropdown.Item onClick={() => { setSortRule("genre"); setSortDir("asc") }}> Genre (Asc.)</Dropdown.Item>
                    <Dropdown.Item onClick={() => { setSortRule("genre"); setSortDir("desc") }}> Genre (Desc.)</Dropdown.Item>

                </Dropdown.Menu>
            </Dropdown>

            <br></br>
            <Dropdown>
                <Dropdown.Toggle id="dropdown-basic" size="lg" variant="dark">
                    Filter By
                </Dropdown.Toggle>
                <Dropdown.Menu>
                    <Dropdown.Item onClick={() => {setFilterString("name||h")}}> Albums A-G</Dropdown.Item>
                    <Dropdown.Item onClick={() => {setFilterString("name|h|o")}}> Albums H-N</Dropdown.Item>
                    <Dropdown.Item onClick={() => {setFilterString("name|o|v")}}> Albums O-U</Dropdown.Item>
                    <Dropdown.Item onClick={() => {setFilterString("name|v|")}}> Albums V-Z</Dropdown.Item>

                    <Dropdown.Item onClick={() => {setFilterString("artist_name||h")}}> Artists A-G</Dropdown.Item>
                    <Dropdown.Item onClick={() => {setFilterString("artist_name|h|o")}}> Artists H-N</Dropdown.Item>
                    <Dropdown.Item onClick={() => {setFilterString("artist_name|o|v")}}> Artists O-U</Dropdown.Item>
                    <Dropdown.Item onClick={() => {setFilterString("artist_name|v|")}}> Artists V-Z</Dropdown.Item>

                    <Dropdown.Item onClick={() => {setFilterString("genre||g")}}> Genres A-G</Dropdown.Item>
                    <Dropdown.Item onClick={() => {setFilterString("genre|h|o")}}> Genres H-N</Dropdown.Item>
                    <Dropdown.Item onClick={() => {setFilterString("genre|o|v")}}> Genres O-U</Dropdown.Item>
                    <Dropdown.Item onClick={() => {setFilterString("genre|v|")}}> Genres V-Z</Dropdown.Item>

                    <Dropdown.Item onClick={() => {setFilterString("year||1971")}}> Before 1970</Dropdown.Item>
                    <Dropdown.Item onClick={() => {setFilterString("year|1971|1981")}}> Before 1980</Dropdown.Item>
                    <Dropdown.Item onClick={() => {setFilterString("year|1981|1991")}}> Before 1990</Dropdown.Item>
                    <Dropdown.Item onClick={() => {setFilterString("year|1991|2001")}}> Before 2000</Dropdown.Item>
                    <Dropdown.Item onClick={() => {setFilterString("year|2001|2011")}}> Before 2010</Dropdown.Item>
                    <Dropdown.Item onClick={() => {setFilterString("year|2011|")}}> After 2010</Dropdown.Item>

                    <Dropdown.Item onClick={() => {setFilterString("num_songs||6")}}> Songs {'<='} 5</Dropdown.Item>
                    <Dropdown.Item onClick={() => {setFilterString("num_songs|6|11")}}> 5 {'<'} Songs {'<='} 10</Dropdown.Item>
                    <Dropdown.Item onClick={() => {setFilterString("num_songs|11|26")}}>  10 {'<'} Songs {'<='} 25</Dropdown.Item>
                    <Dropdown.Item onClick={() => {setFilterString("num_songs|26|51")}}>  25 {'<'} Songs {'<='} 50</Dropdown.Item>
                    <Dropdown.Item onClick={() => {setFilterString("sum_songs|51|")}}> Songs {'>'} 50</Dropdown.Item>

                </Dropdown.Menu>
            </Dropdown>

            <br></br>
            <Form
                inline
                onSubmit={(sub) => {
                    sub.preventDefault();
                }}>
                <FormControl
                    className="model-search"
                    placeholder="Search"
                    type="text"
                    ref={userInput}
                    onKeyPress={(event) => {
                        if (event.key === "Enter") {
                            setSearchString(userInput.current.value);
                        }
                    }}>
                </FormControl>
                <Button
                    className="search-submit"
                    variant="dark"
                    onClick={() => setSearchString(userInput.current.value)}>Submit</Button>
            </Form>
        </Jumbotron>

    return (
        <Container fluid style={{ margin: 0, padding: 0 }}>
            {NormalJumbo}
            <Container fluid style={{ margin: 0, padding: 0 }}>
                <DataTable columns={columns} data={data} fetchData={fetchData} pageCount={numPages} />
            </Container>
        </Container>
    )
}


export default AlbumsPage;