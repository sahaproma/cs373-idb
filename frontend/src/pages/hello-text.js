import { useEffect, useState } from 'react';
import axios from 'axios';
import Artist from './Artists/Artists';

const HelloText = () => {
    const [data, setData] = useState("");

    useEffect(() => {
        const getData = async () => {
            const res = await axios.get(`http://localhost:5000/song`);
            setData(res.data);
            console.log(res.data);
        };
        getData();

    }, [])

    return (
      <Artist data={data} ></Artist>
    )
}

export default HelloText;