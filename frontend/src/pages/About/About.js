import { useEffect, useState } from 'react';
import { Card, CardDeck, Row, Col, Container, Jumbotron, Button } from 'react-bootstrap';
import MemberCard from './memberCard';
import ResourceCard from './resourceCard';
import ToolCard from './toolCard';
import { resources } from '../../assets/data/resources';
import { members } from '../../assets/data/members';
import { tools } from '../../assets/data/tools';
import { important } from '../../assets/data/important';
import axios from 'axios';
import '../../styles/About.css';



const AboutPage = () => {
    const [testOutput, setTestOutput] = useState('');

    const commitTotals = members[0].commits + members[1].commits + members[2].commits +
        members[3].commits + members[4].commits;
    const testTotals = members[0].tests + members[1].tests + members[2].tests +
        members[3].tests + members[4].tests;
    const issueTotals = members[0].issues + members[1].issues + members[2].issues +
        members[3].issues + members[4].issues;

    const fetchData = async () => {
        setTestOutput("Loading test output... (Takes ~10 seconds)");
        const res = await axios.get(`${process.env.REACT_APP_API_URL}/test`);
        console.log(res.data);
        setTestOutput(res.data);
    }

    return (
        // <Container fluid>
        <Container fluid style={{ padding: 0 }}>

            <Jumbotron className="jumbotron jumbotron-change jumbotron-top">
                <Container>
                    <h1 className="display-2">About Us</h1>
                    <p className='aboutparagraph'>
                       MusicMuse is a website that gives information about music on the internet.
                        We are located at the heart of Austin, Texas, known for its live-music in
                        country, blues, and rock-and-roll. As college students living in this amazing
                        city, we would like our website to spread our love for music to everyone around
                        the world. We hope that you'll share your favorite songs on here with your loved ones.
                    </p>            
                </Container>
            </Jumbotron>

            <Container fluid style={{ padding: '2em' }}>
                {/* our personal information */}
                <h2 className="header">Meet The Team</h2>
                <div className="meet">
                    <CardDeck className="member-deck">
                        {members.map((member) => (
                            <MemberCard
                                name={member.name}
                                job={member.job}
                                bio={member.bio}
                                commits={member.commits}
                                issues={member.issues}
                                tests={member.tests}
                                pic={member.pic}>
                            </MemberCard>
                        ))}
                    </CardDeck>
                </div>

                {/* total commits, issues, and unit tests */}
                <Row style={{ paddingTop: '1em' }} className="stats-row">
                    <Col className="stats-col">
                        <h4>Total Commits: {commitTotals}</h4>
                    </Col>
                    <Col className="stats-col">
                        <h4>Total Issues: {issueTotals}</h4>
                    </Col>
                    <Col className="stats-col">
                        <h4>Total Tests: {testTotals}</h4>
                    </Col>
                </Row>
            </Container>

            <Container fluid style={{ padding: '2em' }}>
                {/* api and data sources information */}
                <div style={{ padding: '2em' }}>
                    <CardDeck className="data-deck">
                        {important.map((elem) => (
                            <ToolCard
                                name={elem.name}
                                about={elem.about}
                                link={elem.link}
                                pic={elem.pic}
                            >
                            </ToolCard>
                        ))}
                    </CardDeck>
                </div>
            </Container>

            <Container fluid style={{ padding: '2em' }}>
                {/* Run unit tests*/}
                <h2 className="header">Run Backend Unit Tests</h2>
                <Jumbotron>
                    <Button onClick={fetchData} size="lg">
                       Run Tests (Please be patient!)
                    </Button>
                    <p>{testOutput}</p>
                </Jumbotron>
            </Container>

            <Container fluid style={{ padding: '2em' }}>
                {/* api and data sources information */}
                <div style={{ padding: '2em' }}>
                    <h2 className='header'>Our APIs and Data Sources</h2>
                    <CardDeck className="data-deck">
                        {resources.map((resource) => (
                            <ResourceCard
                                name={resource.name}
                                about={resource.about}
                                link={resource.link}
                                pic={resource.pic}
                                description={resource.description}
                                how={resource.how}
                            >
                            </ResourceCard>
                        ))}
                    </CardDeck>
                </div>
            </Container>

            <Container fluid style={{ padding: '2em' }}>
                {/* information about the tools we used */}
                <h2 className='header'>Our Tools</h2>
                <CardDeck className="tool-deck">
                    {tools.map((tool) => (
                        <ToolCard
                            name={tool.name}
                            use={tool.use}
                            link={tool.link}
                            pic={tool.pic}
                        >
                        </ToolCard>
                    ))}
                </CardDeck>
            </Container>
        </Container>
    );
}

export default AboutPage;
