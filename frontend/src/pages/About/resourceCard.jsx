import React from 'react';
import { Card } from "react-bootstrap";
import '../../styles/About.css';

/* card to display resource information */
const ResourceCard = ({ name, about, link, pic, description, how }) => {
    return (
        <Card className='data-card'>
            <a href={link} target='_blank' rel="noreferrer">
                <Card.Img style={{objectFit: "cover"}} className='data-img' variant='top' src={pic}></Card.Img>
            </a>
            <Card.Text style={{ paddingTop: '10px' }}>
                <a href={link}><b>{name}</b></a>
                <p>{about}</p>
                <p>{description}</p>
                <p>{how}</p>
            </Card.Text>
        </Card>

    );
};

export default ResourceCard;
