import React from 'react'
import { useEffect, useState } from 'react';
import { Image, Container, Jumbotron, Button, Toast, Spinner, Row, Dropdown, Form, FormControl } from "react-bootstrap";
import Highlighter from "react-highlight-words";
import DataTable from '../../components/table';
import axios from 'axios';

const TopSongsPage = ({ selectedSongId }) => {
    const [data, setData] = useState([]);
    const [numPages, setNumPages] = useState(1);
    const [sortRule, setSortRule] = useState("id");
    const [sortDir, setSortDir] = useState("asc");
    const [curPage, setCurPage] = useState(1);
    const [filterString, setFilterString] = useState("");
    const [searchString, setSearchString] = useState("");
    const userInput = React.useRef();

    var fetchData = async ({ pageNum = curPage } = {}) => {
        var params = {
            pageNum: pageNum,
            perPage: 25,
            sortRule: sortRule,
            sortDir: sortDir,
            filter: filterString,
            q: searchString
        }
        const res = await axios.get(`${process.env.REACT_APP_API_URL}/song`, { params });
        console.log(res.data);
        setData(res.data.songs);
        setNumPages(res.data.totalPages);
        setCurPage(res.data.page);
    };

    React.useEffect(() => {
        fetchData()
    }, [sortDir, sortRule, filterString, searchString])

    function highlight(str) {
        return (
            <Highlighter
                highlightClassName="highlightClass"
                searchWords={[searchString]}
                autoEscape={true}
                textToHighlight={str}
            />
        )
    }

    //********************************************************
    //DEFINE YOUR COLUMNS FOR THE TABLE HERE
    //********************************************************
    const columns = React.useMemo(
        () => [
            {
                Header: 'Songs',
                columns: [
                    {
                        Header: 'Title',
                        //update this to /artists/ or /songs
                        accessor: 'name',
                        Cell: (Row) => {
                            var elemId = Row.row.original.id
                            var name = Row.row.original.name
                            return (<a href={"/songs/" + elemId}>
                                {highlight(name)}
                            </a>)
                        },
                    },
                    {
                        Header: 'Artist',
                        accessor: 'artist',
                        Cell: ({ cell: { value } }) => {
                            if (value.id) {
                                //update this to /artists/ or /songs
                                return (<a href={"/artists/" + value.id}>{highlight(value.name)}</a>)
                            }
                            else {
                                return (value.name)
                            }
                        }
                    },
                    {
                        Header: 'Album',
                        accessor: 'album',
                        Cell: ({ cell: { value } }) => {
                            if (value.id) {
                                //update this to /artists/ or /songs
                                return (<a href={"/albums/" + value.id}>{highlight(value.name)}</a>)
                            }
                            else {
                                return (value.name)
                            }
                        }
                    },
                    {
                        Header: 'Genre',
                        accessor: 'genre',
                        Cell: ({ cell: { value } }) => {
                            return highlight(value)
                        }
                    },
                    {
                        Header: 'Length (Seconds)',
                        accessor: 'length',
                        Cell: ({ cell: { value } }) => {
                            if (value <= 0) {
                                //update this to /artists/ or /songs
                                return ("N/A")
                            }
                            else {
                                return value
                            }
                        }
                    }
                ],
            }
        ],
        [searchString]
    )

    const NormalJumbo =
        <Jumbotron style={{ marginBottom: 0 }}>
            <h1>Browse All Songs</h1>
            <p>
                Jam out to the hottest songs!
            </p>
            <Dropdown>
                <Dropdown.Toggle id="dropdown-basic" size="lg" variant="dark">
                    Sort By
                </Dropdown.Toggle>

                <Dropdown.Menu>
                    <Dropdown.Item onClick={() => { setSortRule("name"); setSortDir("asc") }}> Title (Asc.)</Dropdown.Item>
                    <Dropdown.Item onClick={() => { setSortRule("name"); setSortDir("desc") }}> Title (Desc.)</Dropdown.Item>


                    <Dropdown.Item onClick={() => { setSortRule("artist_name"); setSortDir("asc") }}> Artist (Asc.)</Dropdown.Item>
                    <Dropdown.Item onClick={() => { setSortRule("artist_name"); setSortDir("desc") }}> Artist (Desc.)</Dropdown.Item>

                    <Dropdown.Item onClick={() => { setSortRule("album_name"); setSortDir("asc") }}> Album (Asc.)</Dropdown.Item>
                    <Dropdown.Item onClick={() => { setSortRule("album_name"); setSortDir("desc") }}> Album (Desc.)</Dropdown.Item>


                    <Dropdown.Item onClick={() => { setSortRule("length"); setSortDir("asc") }}> Length (Asc.)</Dropdown.Item>
                    <Dropdown.Item onClick={() => { setSortRule("length"); setSortDir("desc") }}> Length (Desc.)</Dropdown.Item>

                    <Dropdown.Item onClick={() => { setSortRule("genre"); setSortDir("asc") }}> Genre (Asc.) </Dropdown.Item>
                    <Dropdown.Item onClick={() => { setSortRule("genre"); setSortDir("desc") }}> Genre (Desc.)</Dropdown.Item>

                </Dropdown.Menu>
            </Dropdown>

            <br></br>

            <Dropdown>
                <Dropdown.Toggle id="dropdown-basic" size="lg" variant="dark">
                    Filter By
                </Dropdown.Toggle>
                <Dropdown.Menu>
                    <Dropdown.Item onClick={() => { setFilterString("name||h") }}> Songs A-G</Dropdown.Item>
                    <Dropdown.Item onClick={() => { setFilterString("name|h|o") }}> Songs H-N</Dropdown.Item>
                    <Dropdown.Item onClick={() => { setFilterString("name|o|v") }}> Songs O-U</Dropdown.Item>
                    <Dropdown.Item onClick={() => { setFilterString("name|v|") }}> Songs V-Z</Dropdown.Item>

                    <Dropdown.Item onClick={() => { setFilterString("artist_name||h") }}> Artists A-G</Dropdown.Item>
                    <Dropdown.Item onClick={() => { setFilterString("artist_name|h|o") }}> Artists H-N</Dropdown.Item>
                    <Dropdown.Item onClick={() => { setFilterString("artist_name|o|v") }}> Artists O-U</Dropdown.Item>
                    <Dropdown.Item onClick={() => { setFilterString("artist_name|v|") }}> Artists V-Z</Dropdown.Item>

                    <Dropdown.Item onClick={() => { setFilterString("album_name||h") }}> Albums A-G</Dropdown.Item>
                    <Dropdown.Item onClick={() => { setFilterString("album_name|h|o") }}> Albums H-N</Dropdown.Item>
                    <Dropdown.Item onClick={() => { setFilterString("album_name|o|v") }}> Albums O-U</Dropdown.Item>
                    <Dropdown.Item onClick={() => { setFilterString("album_name|v|") }}> Albums V-Z</Dropdown.Item>

                    <Dropdown.Item onClick={() => { setFilterString("genre||g") }}> Genres A-G</Dropdown.Item>
                    <Dropdown.Item onClick={() => { setFilterString("genre|h|o") }}> Genres H-N</Dropdown.Item>
                    <Dropdown.Item onClick={() => { setFilterString("genre|o|v") }}> Genres O-U</Dropdown.Item>
                    <Dropdown.Item onClick={() => { setFilterString("genre|v|") }}> Genres V-Z</Dropdown.Item>


                    <Dropdown.Item onClick={() => { setFilterString("length||61") }}> Song length {'<='} 60 sec</Dropdown.Item>
                    <Dropdown.Item onClick={() => { setFilterString("length|61|121") }}> Song length {'<='} 120 sec</Dropdown.Item>
                    <Dropdown.Item onClick={() => { setFilterString("length|121|181") }}>  Song length {'<='} 180 sec</Dropdown.Item>
                    <Dropdown.Item onClick={() => { setFilterString("length|181|241") }}>  Song length {'<='} 240 sec</Dropdown.Item>
                    <Dropdown.Item onClick={() => { setFilterString("length|241|301") }}>  Song length {'<='} 300 sec</Dropdown.Item>
                    <Dropdown.Item onClick={() => { setFilterString("length|301|") }}>  Song length {'>'} 300 sec</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>

            <br></br>
            <Form
                inline
                onSubmit={(sub) => {
                    sub.preventDefault();
                }}>
                <FormControl
                    className="model-search"
                    placeholder="Search"
                    type="text"
                    ref={userInput}
                    onKeyPress={(event) => {
                        if (event.key === "Enter") {
                            setSearchString(userInput.current.value);
                        }
                    }}>
                </FormControl>
                <Button
                    className="search-submit"
                    variant="dark"
                    onClick={() => setSearchString(userInput.current.value)}>Submit</Button>
            </Form>

        </Jumbotron>

    return (
        <Container fluid style={{ margin: 0, padding: 0 }}>
            {NormalJumbo}
            <Container fluid style={{ margin: 0, padding: 0 }}>
                <DataTable columns={columns} data={data} fetchData={fetchData} pageCount={numPages} />
            </Container>
        </Container>
    )
}

export default TopSongsPage;