import React from 'react'
import { useEffect, useState } from 'react';
import { Image, Container, Jumbotron, Button, Toast, Spinner, Row, Dropdown } from "react-bootstrap";
import { useParams, useHistory } from "react-router-dom";
import EmbeddedVideo from '../../components/embeddedVideo';
import axios from 'axios';

const SongPage = () => {
    const history = useHistory();
    const { id } = useParams();
    const [data, setData] = useState(null);

    useEffect(() => {
        fetchData()
    }, [])

    const fetchData = async () => {
        const res = await axios.get(`${process.env.REACT_APP_API_URL}/song/${id}`);
        console.log(res.data);
        setData(res.data);
    };

    //bootstrap banner for song name

    function loadSongData() {
        return (
            <Container fluid style={{ margin: 0, padding: 0 }}>
                <Jumbotron>
                    <h1 className="name">{data.name}</h1>
                    <p>Genre: {data.genre}</p>
                    <p>Length: {data.length > 0 ? data.length : "N/A"}</p>
                    <p>Artist: <a href={"../artists/" + data.artist.id}>{data.artist.name}</a> </p>
                    <p>Album: <a href={"../albums/" + data.album.id}>{data.album.name}</a> </p>
                    <Container fluid style={{ margin: 0, padding: 0 }}>
                        <EmbeddedVideo embedURL={data.videoEmbedURL} />
                    </Container>

                    <p style={{marginTop: 20}}>
                        <Button onClick={() => { history.push('/songs') }}>View All Songs</Button>
                    </p> 
                </Jumbotron>
            </Container>
        )
    }

    return (
        <Container fluid style={{ margin: 0, padding: 0 }}>
            {data != null ? loadSongData() : ""}
        </Container>
    )
}

export default SongPage;