import { useEffect, useState } from "react";
import axios from "axios";
import Chart from "react-google-charts";
import { Container, Jumbotron } from "react-bootstrap";

const Group8Page = () => {
  const [data, setData] = useState("");

  useEffect(() => {
    const getData = async () => {
      const res = await axios.get(
        `${process.env.REACT_APP_API_URL}/players`
      );
      console.log(res.data);
      setData(res.data);
    };
    getData();
  }, []);
  
  function getRandomColor() {
    const letters = "0123456789ABCDEF";
    let color = "#";
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  const getTopScorerData = () => {
    //foreach loop through data.topScorers
    //get name, score, and total games played
    //return array of data
    const topScorers = [];
    const Columns = [ "Name", "Average Points Per Game", { role: 'style' }];
    topScorers.push(Columns);
    data.topScorers.data.forEach(player => {
      const name = player.name;
      const avgPoints = player.average_points_per_game;
      const playerData = [ name, avgPoints, getRandomColor()];
      topScorers.push(playerData);
    });
    console.log(topScorers);
    return topScorers;
  }

  //function that returns a div if data is loaded
  const getDiv = () => {
    if (data) {
      return (
        <div className="chartContainer">
          <Chart
            style={{ padding: "20px" }}
            width={'60vw'}
            height={'80vh'}
            chartType="BarChart"
            data={getTopScorerData()}
            options={{
              title: "Top Scorers In the NBA",
            }}
          />
        </div>
      );
    }
    return (<div>Loading Chart...</div>);
  };

  return (
    <Container fluid style={{ margin: 0, padding: 0 }}>
      <Jumbotron>
        <p>Due to Group 8's API being down, we are using Group 6's API</p>
        {getDiv()}
      </Jumbotron>
    </Container>
  );
};

export default Group8Page;
