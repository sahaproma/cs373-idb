import React from 'react'
import { useEffect, useState } from 'react';
import { Image, Container, Jumbotron, Button, Toast, Spinner, Row, Dropdown, ListGroup } from "react-bootstrap";
import { useParams, useHistory } from "react-router-dom";
import axios from 'axios';

const ArtistPage = () => {
    const history = useHistory();
    const { id } = useParams();
    const [data, setData] = useState(null);

    useEffect(() => {
        fetchData()
    }, [])

    const fetchData = async () => {
        const res = await axios.get(`${process.env.REACT_APP_API_URL}/artist/${id}`);
        console.log(res.data);
        setData(res.data);
     };

     function loadArtistData() {
        console.log(data);
        return (
            <Container fluid style={{ margin: 0, padding: 0 }}>
                <Jumbotron>
                    <Container fluid>
                        <h1 className="name">{data.name}</h1>
                        <Image fluid rounded src={data.picture}></Image>
                        <p>Genre: {data.genre}</p>
                        <p>Active From: {data.activeFrom}</p>
                        <p># of Songs: {data.numSongs}</p>
                        <h2 className="name">Albums</h2>
                        <ListGroup>
                        {data.albums.map((elem) => (
                            <ListGroup.Item><a href={`/albums/${elem.id}`}> {elem.name} </a></ListGroup.Item>
                        ))}
                        </ListGroup>
                        
                        <h2 className="name">Songs</h2>
                        <ListGroup>
                        //sort allSongs by name
                        {data.allSongs.sort((a,b) => {  
                            if (a.name < b.name) {
                                return -1;
                            }
                            if (a.name > b.name) {
                                return 1;
                            }
                            return 0;
                        }).map((elem) => (
                            <ListGroup.Item><a href={`/songs/${elem.id}`}> {elem.name} </a></ListGroup.Item>
                        ))}
                        </ListGroup>

                        <p style={{marginTop: 20}}>
                            <Button onClick={() => { history.push('/artists') }}>View All Artists</Button>
                        </p> 
                    </Container>
                </Jumbotron>
            </Container>
        )
    }

    return (
        <Container fluid style={{ margin: 0, padding: 0 }}>
            <Container fluid style={{ margin: 0, padding: 0 }}>
                {data != null ? loadArtistData() : ""}
            </Container>
        </Container>
    )
}

export default ArtistPage;