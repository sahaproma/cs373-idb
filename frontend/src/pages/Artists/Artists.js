import React from 'react'
import { useEffect, useState } from 'react';
import { Image, Container, Jumbotron, Button, Toast, Spinner, Row, Dropdown, Form, FormControl } from "react-bootstrap";
import DataTable from '../../components/table';
import Highlighter from "react-highlight-words";
import axios from 'axios';

const ArtistsPage= () => {
    const [data, setData] = useState([]);
    const [numPages, setNumPages] = useState(1);
    const [sortRule, setSortRule] = useState("id");
    const [sortDir, setSortDir] = useState("asc");
    const [curPage, setCurPage] = useState(1);
    const [filterString, setFilterString] = useState("");
    const [searchString, setSearchString] = useState("");
    const userInput = React.useRef();

    var fetchData = async ({pageNum = curPage} = {}) => {
        var params = {
            pageNum: pageNum,
            perPage: 10,
            sortRule: sortRule,
            sortDir: sortDir,
            filter: filterString,
            q: searchString
        }
        const res = await axios.get(`${process.env.REACT_APP_API_URL}/artist`, { params });
        console.log(res.data);
        setData(res.data.artists);
        setNumPages(res.data.totalPages);
        setCurPage(res.data.page);
    };

    function highlight(str) {
        return (
            <Highlighter
                highlightClassName="highlightClass"
                searchWords={[searchString]}
                autoEscape={true}
                textToHighlight={str}
            />
        )
    }

    React.useEffect(() => {
        fetchData()
    }, [sortDir, sortRule, filterString, searchString])

    //********************************************************
    //DEFINE YOUR COLUMNS FOR THE TABLE HERE
    //********************************************************
    const columns = React.useMemo(
        () => [
            {
                Header: 'Artists',
                columns: [
                    {
                        Header: 'Name',
                        //update this to /artists/ or /songs
                        accessor: 'name',
                        Cell: (Row) => {
                            var elemId = Row.row.original.id
                            var name = Row.row.original.name
                            return(<a href={"/artists/" + elemId}>{highlight(name)}</a>)
                        },
                    },
                    {
                        Header: "Image",
                        accessor: "picture",
                        Cell: ({ cell: { value } }) => (
                            <Image
                                src={value}
                                width={150}
                                rounded
                            />
                        )
                    },
                    {
                        Header: 'Genre',
                        accessor: 'genre',
                        Cell: ({ cell: { value } }) => {
                            return highlight(value)
                        }
                    },
                    {
                        Header: 'Number of Albums',
                        accessor: 'albums',
                        Cell: ({ cell: { value } }) => {
                            return highlight(value.length.toString())
                        }
                    },
                    {
                        Header: 'Number of Songs',
                        accessor: 'numSongs',
                        Cell: ({ cell: { value } }) => {
                            return highlight(value.toString())
                        }
                    },
                    {
                        Header: 'Active From',
                        accessor: 'activeFrom',
                        Cell: ({ cell: { value } }) => {
                            return highlight(value.toString())
                        }
                    }
                ],
            }
        ],
        [searchString]
    )

    const NormalJumbo =
        <Jumbotron style={{ marginBottom: 0 }}>
            <h1>Browse All Artists</h1>
            <p>
                Find your favorite artists!
            </p>
                <Dropdown>
                        <Dropdown.Toggle id="dropdown-basic" size="lg" variant="dark">
                            Sort By
                        </Dropdown.Toggle>

                        <Dropdown.Menu>
                            <Dropdown.Item onClick={() => {setSortRule("name"); setSortDir("asc")}}> Name (Asc.)</Dropdown.Item>
                            <Dropdown.Item onClick={() => {setSortRule("name"); setSortDir("desc")}}> Name (Desc.)</Dropdown.Item>


                            <Dropdown.Item onClick={() => {setSortRule("num_songs"); setSortDir("asc")}}> # of Songs (Asc.) </Dropdown.Item>
                            <Dropdown.Item onClick={() => {setSortRule("num_songs"); setSortDir("desc")}}> # of Songs (Desc.)</Dropdown.Item>

                            <Dropdown.Item onClick={() => {setSortRule("num_albums"); setSortDir("asc")}}> # of Albums (Asc.)</Dropdown.Item>
                            <Dropdown.Item onClick={() => {setSortRule("num_albums"); setSortDir("desc")}}> # of Albums (Asc.)</Dropdown.Item>


                            <Dropdown.Item onClick={() => {setSortRule("active_from"); setSortDir("asc")}}> Active From (Asc.)</Dropdown.Item>
                            <Dropdown.Item onClick={() => {setSortRule("active_from"); setSortDir("desc")}}> Active From (Desc.)</Dropdown.Item>

                            <Dropdown.Item onClick={() => {setSortRule("main_genre"); setSortDir("asc")}}> Genre (Asc.) </Dropdown.Item>
                            <Dropdown.Item onClick={() => {setSortRule("main_genre"); setSortDir("desc")}}> Genre (Desc.)</Dropdown.Item>

                        </Dropdown.Menu>
                    </Dropdown>

                    <br></br>

                    <Dropdown>
                        <Dropdown.Toggle id="dropdown-basic" size="lg" variant="dark">
                            Filter By
                        </Dropdown.Toggle>
                        <Dropdown.Menu>
                        
                            <Dropdown.Item onClick={() => {setFilterString("name||h")}}> Artists A-G</Dropdown.Item>
                            <Dropdown.Item onClick={() => {setFilterString("name|h|o")}}> Artists H-N</Dropdown.Item>
                            <Dropdown.Item onClick={() => {setFilterString("name|o|v")}}> Artists O-U</Dropdown.Item>
                            <Dropdown.Item onClick={() => {setFilterString("name|v|")}}> Artists V-Z</Dropdown.Item>

                            <Dropdown.Item onClick={() => {setFilterString("main_genre||g")}}> Genres A-G</Dropdown.Item>
                            <Dropdown.Item onClick={() => {setFilterString("main_genre|h|o")}}> Genres H-N</Dropdown.Item>
                            <Dropdown.Item onClick={() => {setFilterString("main_genre|o|v")}}> Genres O-U</Dropdown.Item>
                            <Dropdown.Item onClick={() => {setFilterString("main_genre|v|")}}> Genres V-Z</Dropdown.Item>

                            <Dropdown.Item onClick={() => {setFilterString("num_albums||6")}}> Albums {'<='} 5</Dropdown.Item>
                            <Dropdown.Item onClick={() => {setFilterString("num_albums|6|11")}}> 5 {'<'} Albums {'<='} 10</Dropdown.Item>
                            <Dropdown.Item onClick={() => {setFilterString("num_albums|11|26")}}>  10 {'<'} Albums {'<='} 25</Dropdown.Item>
                            <Dropdown.Item onClick={() => {setFilterString("num_albums|26|")}}> Albums {'>'} 25</Dropdown.Item>

                            <Dropdown.Item onClick={() => {setFilterString("num_songs||26")}}> Songs {'<='} 25</Dropdown.Item>
                            <Dropdown.Item onClick={() => {setFilterString("num_songs|26|101")}}> 25 {'<'} Songs {'<='} 100</Dropdown.Item>
                            <Dropdown.Item onClick={() => {setFilterString("num_songs|101|251")}}>  100 {'<'} Songs {'<='} 250</Dropdown.Item>
                            <Dropdown.Item onClick={() => {setFilterString("num_songs|251|")}}> Songs {'>'} 250</Dropdown.Item>

                            <Dropdown.Item onClick={() => {setFilterString("active_from||1961")}}> Active Before 1960</Dropdown.Item>
                            <Dropdown.Item onClick={() => {setFilterString("active_from|1961|1971")}}>Active Before 1970</Dropdown.Item>
                            <Dropdown.Item onClick={() => {setFilterString("active_from|1971|1981")}}> Active Before 1980</Dropdown.Item>
                            <Dropdown.Item onClick={() => {setFilterString("active_from|1981|1991")}}> Active Before 1990</Dropdown.Item>
                            <Dropdown.Item onClick={() => {setFilterString("active_from|1991|2001")}}> Active Before 2000</Dropdown.Item>
                            <Dropdown.Item onClick={() => {setFilterString("active_from|2001|2011")}}> Active Before 2010</Dropdown.Item>
                            <Dropdown.Item onClick={() => {setFilterString("active_from|2011|")}}> Active After 2010</Dropdown.Item>

                        </Dropdown.Menu>
                    </Dropdown>

                    <br></br>
                    <Form
                        inline
                        onSubmit={(sub) => {
                            sub.preventDefault();
                        }}>
                        <FormControl
                            className = "model-search"
                            placeholder="Search"
                            type="text"
                            ref={userInput}
                            onKeyPress={(event) => {
                                if (event.key === "Enter") {
                                    setSearchString(userInput.current.value);
                                }
                            }}>
                        </FormControl>
                        <Button
                            className = "search-submit"
                            variant="dark"
                            onClick={() => setSearchString(userInput.current.value)}>Submit</Button>
                    </Form>
        </Jumbotron>

    return (
        <Container fluid style={{ margin: 0, padding: 0 }}>
            {NormalJumbo}
            <Container fluid style={{ margin: 0, padding: 0 }}>
                <DataTable columns={columns} data={data} fetchData={fetchData} pageCount={numPages}/>
            </Container>
        </Container>
    )
}

export default ArtistsPage;