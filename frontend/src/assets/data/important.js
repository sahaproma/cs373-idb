/* importing tools pictures */
import postmanPic from '../images/logos/postman-logo.png';
import gitlabPic from '../images/logos/gitlab-logo.png';
import speackerdeckPic from '../images/logos/speakerdeck-logo.png';

/* information for tools for about page */
export const important = [
    {
        name: "Our Postman API",
        link: "https://documenter.getpostman.com/view/16330177/TzeahQZ7",
        pic: postmanPic,
    },
    {
        name: "Our Gitlab Repo",
        link: "https://gitlab.com/kev.guo123/cs373-idb",
        pic: gitlabPic,
    },
    {
        name: "Issue Tracker",
        link: "https://gitlab.com/kev.guo123/cs373-idb/-/issues",
        pic: gitlabPic,
    },
    {
        name: "GitLab Wiki",
        link: "https://gitlab.com/kev.guo123/cs373-idb/-/wikis/MusicMuse",
        pic: gitlabPic,
    },
    {
        name: "Speaker Deck",
        link: "https://speakerdeck.com/tuaniiboyy/group-7-presentation",
        pic: speackerdeckPic,
    },
]
