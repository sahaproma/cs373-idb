export const albumData = {
    "size": 3,
    "albums": [
        {
            name: "Fearless",
            genre: "Country Pop",
            artist: {
                "id": 1,
                "name": "Taylor Swift"
            },
            year: 2008,
            songs: [
                {
                    "id": 1,
                    "name": "Love Story"
                }
            ],
            picture: "https://upload.wikimedia.org/wikipedia/en/8/86/Taylor_Swift_-_Fearless.png",
            id: 1
        },
        {
            name: "Views",
            genre: "Rap",
            artist: {
                "id": 2,
                "name": "Drake"
            },
            year: 2016,
            songs: [
                {
                    "id": 2,
                    "name": "One Dance"
                }
            ],
            picture: "https://thefader-res.cloudinary.com/private_images/w_640,c_limit,f_auto,q_auto:eco/Cg2YkksWMAACMbv_momzvl/drake-views-from-the-6-artwork.jpg",
            id: 2
        },
        {
            name: "Pet Sounds",
            genre: "Rock",
            artist: {
                "id": 3,
                "name": "The Beach Boys"
            },
            year: 1966,
            songs: [
                {
                    "id": 3,
                    "name": "Wouldn't It Be Nice"
                }
            ],
            picture: "https://upload.wikimedia.org/wikipedia/en/b/bb/PetSoundsCover.jpg",
            id: 3
        }
    ]
}