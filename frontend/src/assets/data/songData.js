export const songData = {
    "size": 3,
    "songs": [
        {
            name: "Love Story",
            genre: "Country",
            album: {
                id: 1,
                name: "Fearless"
            },
            id: 1,
            length: 237,
            artist: {
                id: 1,
                name: "Taylor Swift"
            },
            embedURL: "https://www.youtube.com/embed/8xg3vE8Ie_E"
        },
        {
            name: "One Dance",
            genre: "Rap",
            album: {
                id: 2,
                name: "Views"
            },
            id: 2,
            length: 174,
            artist: {
                id: 2,
                name: "Drake"
            },
            embedURL: "https://www.youtube.com/embed/qL7zrWcv6XY"
        },
        {
            name: "Wouldn't It Be Nice",
            genre: "Rock",
            album: {
                id: 3,
                name: "Pet Sounds"
            },
            id: 3,
            length: 144,
            artist: {
                id: 3,
                name: "The Beach Boys"
            },
            embedURL: "https://www.youtube.com/embed/dmcNbsLCpBQ"
        }
    ]
}