/* importing tools pictures */
import postmanPic from '../images/logos/postman-logo.png';
import reactPic from '../images/logos/react-logo.png';
import bootstrapPic from '../images/logos/bootstrap-logo.svg';
import vscodePic from '../images/logos/vscode-logo.png';
import gitlabPic from '../images/logos/gitlab-logo.png';
import slackPic from '../images/logos/slack-logo.png';

/* information for tools for about page */
export const tools = [
    {
        name: "Postman",
        use: "Testing both our own endpoints and the endpoints of the APIs we are using.",
        link: "https://www.postman.com/",
        pic: postmanPic,
    },
    {
        name: "React",
        use: "To code the front-end.",
        link: "https://reactjs.org/",
        pic: reactPic,
    },
    {
        name: "Bootstrap",
        use: "CSS framework for our website.",
        link: "https://getbootstrap.com/",
        pic: bootstrapPic,
    },
    {
        name: "VSCode",
        use: "The main IDE for coding our project.",
        link: "https://code.visualstudio.com/",
        pic: vscodePic,
    },
    {
        name: "Gitlab",
        use: "Tracking our issues, user stories, and CI / CD pipelines.",
        link: "https://gitlab.com/",
        pic: gitlabPic,
    },
    {
        name: "Slack",
        use: "For communication and collaboration.",
        link: "https://slack.com/",
        pic: slackPic,
    }
]
